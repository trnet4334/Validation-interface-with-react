# Validation Interface

## Introduction

This application create through React.js. The purpose of this app is present a prototype of membership login and sign up interface combined with router and validation function.

## Screenshot

![screenshot](/src/image/reactValidation.png) 

## Demo

Check it out [live](https://trnet4334.github.io/Validation-interface-with-react/)!

## Technologies
   
This page was built by using the following technologies: 

* Bootstrap 4 
* reactstrap (via [`reactstrap`](https://github.com/reactstrap/reactstrap))
* React (via [`create-react-app`](https://github.com/facebookincubator/create-react-app))
* JavaScript ES6
* React Router
* fontawesome
* Material UI for React

## Task list

- \[x] Page layout
- \[x] Styling form block
- \[x] Practice React Router
- \[ ] Add authentication on router path
- \[ ] Make whole page responsive
- \[ ] Add validation into input form
- \[ ] Create dashboard after login component