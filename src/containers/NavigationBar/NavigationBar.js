import React, {Component} from 'react';
import {
    Collapse,
    Navbar,
    NavbarBrand,
    NavbarToggler,
    Nav,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';

class NavigationBar extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        return (
            <div>
                <Navbar color="faded" light expand="md">
                    <NavbarBrand href="/">F2E SHOP</NavbarBrand>
                    <NavbarToggler onClick={this.toggle}/>
                    <Collapse isOpen={!this.state.isOpen} navbar>
                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <NavLink>HOME</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink>NEW ARRIVAL</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink>SHOES</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink>APPAREL</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink>EQUIPMENTS</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink>STORE LOCATION</NavLink>
                            </NavItem>
                            <UncontrolledDropdown nav inNavbar>
                                <DropdownToggle nav caret>Admin</DropdownToggle>
                                <DropdownMenu right>
                                    <DropdownItem>Login</DropdownItem>
                                    <DropdownItem>Setting</DropdownItem>
                                    <DropdownItem divider/>
                                    <DropdownItem>Logout</DropdownItem>
                                </DropdownMenu>
                            </UncontrolledDropdown>
                        </Nav>
                    </Collapse>
                </Navbar>
            </div>
        );
    }
}

export default NavigationBar;