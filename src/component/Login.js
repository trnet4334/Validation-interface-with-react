import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Col, Form, Row, Input, Button} from 'reactstrap';
import Header from './Header';
//import Input from './Input';
import '../css/Login.css';


class Login extends Component {
    render() {
        return (
            <div className="login-div">
                <Col sm={{size: 6, offset: 3}}>
                    <br/>
                    <Header title={'Login'} subtitle={'Please use the form below to sign in to your account.'}/>
                    <Form className="login-form">
                        <Row className="login-box1">
                            <label htmlFor="Email"> Email: </label>
                            <Input
                                type={'email'}
                                id={'Email'}
                                placeholder={'example@email.com'}
                            />
                        </Row>
                        <Row className="login-box2">
                            <label htmlFor="password"> Password: </label>
                            <Input
                                type={'password'}
                                id={'Password'}
                                placeholder={'●●●●●●●●'}
                            />
                        </Row>
                        <Row >
                            <Col sm={{size: 'auto', offset: 2}}>
                                <Link to="/">
                                    <Button color="primary">Login</Button>
                                </Link>
                            </Col>
                        </Row>
                    </Form>
                </Col>
                <Link to="/">
                    <Button color="primary">Back</Button>
                </Link>
            </div>
        );
    }
}

export default Login;