import React from 'react';
import { Grid } from '@material-ui/core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSmile } from '@fortawesome/free-solid-svg-icons';
import Header from './Header';

function Complete(){
      return (

              <Grid container xs={12} spacing={32}>
                  <Grid item xs={1}></Grid>
                  <Grid item xs={10}>
                      <Header title={'Congratulations'} subtitle={'Now you’re one of us!'}/>
                  </Grid>
                  <Grid item xs={4}></Grid>
                  <Grid item xs={4}>
                      <FontAwesomeIcon icon={faSmile} color="#BEE1FF" size="10x"/>
                  </Grid>

                  {/*<div style={{color: '#0275D8', fontStyle: 'italic', margin: '30px 0'}}>Redirected to homepage after 5*/}
                      {/*seconds…*/}
                  {/*</div>*/}
              </Grid>

      );
}

export default Complete;
