import React, {Fragment} from 'react';
import {
    Grid,
    Typography,
    TextField,
    FormControlLabel,
    Checkbox
} from '@material-ui/core';

function PaymentForm() {
    return (
        <Fragment>
            <Typography variant="title" gutterBottom style={{textAlign:'center'}}>
            Payment method
            </Typography>
            <Grid container spacing={24}>
                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        id="cardHolder"
                        label="Card holder"
                        fullWidth
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        id="cardNumber"
                        label="Card number"
                        fullWidth
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        id="expireDate"
                        label="Expiration date"
                        fullWidth
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <TextField
                        required
                        id="cvv"
                        label="CVV"
                        fullWidth
                        helperText="Last three digits on card back"
                    />
                </Grid>
                <Grid item xs={2}></Grid>
                <Grid item xs={10}>
                    <FormControlLabel
                        control={<Checkbox color="secondary" name="saveCard" value="yes"/>}
                        label="Remember card info for next payment"
                    />
                </Grid>
            </Grid>
        </Fragment>
    );
}

export default PaymentForm;